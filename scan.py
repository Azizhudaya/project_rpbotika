#! /usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan

class ScanWrapper:
    def callback(self,msg):
        self.right = msg.ranges[0]
        self.foward = msg.ranges[255]
        self.left = msg.ranges[511]
        self.backward = msg.ranges[767]

    def get_foward_distance(self):
        return self.foward
    
    def get_backward_distance(self):
        return self.backward

    def get_right_distance(self):
        return self.right

    def get_left_distance(self):
        return self.left

    def __init__(self):
        self.foward = 0
        self.backward = 0
        self.right = 0
        self.left = 0

        rospy.Subscriber('/spur/laser/scan', LaserScan, self.callback)