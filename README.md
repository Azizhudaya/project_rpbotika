# Project Robotika

repository untuk menyimpan kodingan proyek robotika

cara penggunaan:

- Clone repository ini di directory home
- Install semua dependency yang ada di [sini](https://jderobot.github.io/RoboticsAcademy/installation/) Dari awal hingga akhir (intalasi dependency memerlukan waktu ± 3 jam so be patient :))
- buka terminal baru dan jalankan perintah di bawah ini 
    ```bash
    cd ~/project_rpbotika
    roslaunch rescue_people.launch
    ```
- Untuk menjalankan code yang dibuat buka terminal lain dan jalankan perintah ini
    ```bash
    cd ~/project_rpbotika
    python my_solution.py
    ```
- enjoy

