#!/usr/bin/env python
import rospy
import numpy as np
import cv2
from drone_wrapper import DroneWrapper
from std_msgs.msg import Bool, Float64
from sensor_msgs.msg import Image
from geometry_msgs.msg import Twist, Pose
from scan import ScanWrapper

code_live_flag = False
AREA = [[7.0, -4.0], [7.0, 3.0], [-1.0, 7.0], [-7.0, 0.5], [-0.5, -7.0]]
CASCPATH = "haarcascade_frontalface_default.xml"
face_cascade = cv2.CascadeClassifier(CASCPATH)

def set_image_filtered(img):
	gui_filtered_img_pub.publish(drone.bridge.cv2_to_imgmsg(img))

def set_image_threshed(img):
	gui_threshed_img_pub.publish(drone.bridge.cv2_to_imgmsg(img))

def gerak(maju,belok):
	case = round(np.abs(pos['yaw']/(np.pi/2)))%4
		
	if case==0.0:
		drone.set_cmd_pos(pos['x']+maju,pos['y']+belok,pos['z'],pos['yaw'])
		[pos['x'],pos['y'],pos['z']] = [pos['x']+maju,pos['y']+belok,pos['z']]
		rospy.sleep(1)
		print "case0"
	elif case==1.0:
		drone.set_cmd_pos(pos['x']-belok,pos['y']+(maju),pos['z'],pos['yaw'])
		[pos['x'],pos['y'],pos['z']] = [pos['x']-belok,pos['y']+(maju),pos['z']]
		rospy.sleep(1)
		print "case1"
	elif case==2.0:
		drone.set_cmd_pos(pos['x']-maju,pos['y']-belok,pos['z'],pos['yaw'])
		[pos['x'],pos['y'],pos['z']] = [pos['x']-maju,pos['y']-belok,pos['z']]
		rospy.sleep(1)
		print "case2"
	elif case==3.0:
		drone.set_cmd_pos(pos['x']+belok,pos['y']-(maju),pos['z'],pos['yaw'])
		[pos['x'],pos['y'],pos['z']] = [pos['x']+belok,pos['y']-(maju),pos['z']]
		rospy.sleep(1)
		print "case3"
	else:
		print "case = {}".format(case)
		print "error occured"
		print pos['yaw']
		print np.abs(pos['yaw']/(np.pi/2))%4
	

def putar(arah):
	if arah == "kiri":
		drone.set_cmd_pos(pos['x'],pos['y'],pos['z'],pos['yaw'] + np.pi/2)
		pos['yaw'] = pos['yaw']+np.pi/2
		rospy.sleep(2)
		print "kiri"
	if arah == "kanan":
		drone.set_cmd_pos(pos['x'],pos['y'],pos['z'],pos['yaw'] - np.pi/2)
		pos['yaw'] = pos['yaw']+3*np.pi/2
		rospy.sleep(2)
		print "kanan"

def execute():
	global drone
	global status
	global scan

	# Both the above images are cv2 images
	################# Insert your code here #################################
	if status['mode_debug']:
		pass
		
	else:
		status['turn'] = True
		if  status['init']:
			drone.takeoff(2)

			#nit position, bcz i dont like this api
			[pos['x'],pos['y'],pos['z']] = drone.get_position()
			pos['yaw'] = 0

			putar("kanan")
			left_distance = scan.get_left_distance()
			gerak(0,left_distance-1)

			status['init'] = False
			status['turn'] = False

		if status['isFreeMove'] & status['turn']:
			foward_distance = scan.get_foward_distance()

			if(foward_distance < 2):
				status['isFreeMove'] = False
				status['turn'] = False
				status['belok'] = True

			else :
				gerak(1,0)
				
		
		if status['belok']:
			rospy.sleep(3)
			if scan.get_left_distance() < 2:
				putar("kanan")
			elif scan.get_left_distance() > 3.5:
				putar("kiri")
				gerak(3,0)
				putar("kiri")
				gerak(2,0)

			else:
				putar("kanan")

			status['isFreeMove'] = True
			status['turn'] = False
			status['belok'] = False

		#special case
		if scan.get_left_distance() > 3.5 and scan.get_left_distance() < 100:
			status['turn'] = False
			status['belok'] = True
			status['isFreeMove'] = False

	#########################################################################

if __name__ == "__main__":
	drone = DroneWrapper()
	scan = ScanWrapper()
	status = {
		'init' : True,
		'turn': True,
		'isFreeMove' : True,
		'belok' : False,
		'mode_debug': False 
	}
	pos = {
		'x':0,
		'y':0,
		'z':0,
		'yaw':0
	}

	print "masuk"
	while True:
		execute()
